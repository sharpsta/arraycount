//Counts the number of occurrences of a target value in an array of integers
function arrayCount(arr, targetValue) {
	if (!Array.isArray(arr) || arr.length === 0 || targetValue < 0 || isNaN(targetValue)) {
		//Check for invalid input
		return -1;
	}
	
	//Binary search the array until we've found an instance of our target value
	let start = 0, end = arr.length - 1;
	while (start <= end) {
		let mid = start + Math.floor((end - start) / 2); //Value in middle of array
		
		if (arr[mid] == targetValue) {
			//Count the number of occurrences of targetValue once we've found it
			let startIndex = -1, endIndex = -1, d = 1;
			while (startIndex === -1 || endIndex === -1) {
				if (startIndex === -1 && arr[mid - d] < targetValue) {
					//Keep searching left until we reach a value less than our target
					startIndex = (mid - d) + 1;
				}
				
				if (endIndex === -1 && arr[mid + d] > targetValue) {
					//Keep searching right until we reach a value greater than our target
					endIndex = (mid + d), arr.length - 1;
				}
				
				if (startIndex > -1 && endIndex > -1) {
					return endIndex - startIndex;
				} else {
					//Increase our search distance
					d++;
				}
			}
		}
		
		
		if (targetValue < arr[mid]) {
			//Target is on left of mid
			end = mid - 1;
		} else {
			//Target is on right of mid
			start = mid + 1;
		}
	}
	
	//Target value not found
	return -1;
}

const a = [0, 1, 1, 2, 3, 4, 4, 4, 5, 5, 5, 5, 5, 5, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 9, 10];
const t = 7;
document.writeln(`Searching array [${a}]<br />`);
document.writeln(`Target: ${t}<br />`);
document.writeln(`Number of occurrences of target value ${t}:` + arrayCount(a, t));